﻿# Region seed
import numpy as np
import cv2
import math

class RegionSeed(object):

    def __init__(self, img):
        self.img = img

    def dist_euclidiana(self, v1, v2):
            soma = math.pow((int(v1) - int(v2)), 2)
            return math.sqrt(soma)

    def middleImagemSeed(self, img):

        # Linhas
        nLinhas, nColunas = img.shape

        # Dividaso
        nPixelInicial = 1;
        nPixelFinal = nLinhas-1;
        nSeedTop = 0;
        nSeedBottom = 0;

        while(nPixelInicial != nPixelFinal):           

            #dist1 = self.dist_euclidiana(np.sum(img[nPixelInicial][0:nColunas]), np.sum(img[nPixelInicial+1][0:nColunas]))
            #dist2 = self.dist_euclidiana(np.sum(img[nPixelFinal][0:nColunas]), np.sum(img[nPixelFinal-1][0:nColunas]))

            dist1 = self.dist_euclidiana(img[nPixelInicial][1], img[nPixelInicial + 1][1])
            dist2 = self.dist_euclidiana(img[nPixelFinal][1], img[nPixelFinal - 1][1])

            if (dist1 + nSeedTop) < (dist2 + nSeedBottom):
                nPixelInicial = nPixelInicial + 1;
                nSeedTop = nSeedTop + 1;
            else:
                nPixelFinal = nPixelFinal - 1;
                nSeedBottom = nSeedBottom + 1;
        
        return nPixelInicial

    def delimitarImagemLinhaDinamica(self):

        # Delimita o tamanho de cada imagem, necessario para dividir a imagem em três partes

        # Linhas
        nLinhas, nColunas = self.img.shape       

        nMiddlePixel = self.middleImagemSeed(self.img)
        parte1Img = cv2.getRectSubPix(self.img, (nColunas, nMiddlePixel), (1+(nColunas/2), 1+(nMiddlePixel/2)))      
        parte2Img = cv2.getRectSubPix(self.img, (nColunas, (nLinhas - nMiddlePixel)), (1+(nColunas/2), 1+nMiddlePixel+((nLinhas - nMiddlePixel)/2)))

        nMiddlePixelparte1 = self.middleImagemSeed(parte1Img)
        nMiddlePixelparte2 = self.middleImagemSeed(parte2Img)

        nLinhasImg1 = nMiddlePixelparte1
        nLinhasImg2 = (nMiddlePixel - nMiddlePixelparte1) + nMiddlePixelparte2
        nLinhasImg3 = nLinhas - (nLinhasImg1 + nLinhasImg2)

        return nLinhasImg1, nLinhasImg2, nLinhasImg3

    def delimitarImagemLinhaFixa(self):

        # Delimita o tamanho de cada imagem, necessario para dividir a imagem em três partes

        # Linhas
        nLinhas, nColunas = self.img.shape       

        nLinhasImg1 = int(nLinhas / 3)
        nLinhasImg2 = int(nLinhas / 3)
        nLinhasImg3 = nLinhas - (nLinhasImg1 + nLinhasImg2)

        return nLinhasImg1, nLinhasImg2, nLinhasImg3

    def dividirImagem(self, nLinhasImg1, nLinhasImg2, nLinhasImg3):

        # Colunas
        nLinhas, nColunas = self.img.shape

        img1 = cv2.getRectSubPix(self.img, (nColunas, nLinhasImg1), (1+(nColunas/2), 1+(nLinhasImg1/2)))
        img2 = cv2.getRectSubPix(self.img, (nColunas, nLinhasImg2), (1+(nColunas/2), 1+nLinhasImg1+(nLinhasImg2/2)))
        img3 = cv2.getRectSubPix(self.img, (nColunas, nLinhasImg3), (1+(nColunas/2), 1+ nLinhasImg1 + nLinhasImg2 +(nLinhasImg3/2)))

        return img1, img2, img3
