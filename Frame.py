﻿# Frame, objeto que representa o frame

class Frame(object):

    #Definição de atributos
    
    frame = None
    frameanterior = None
    numero = 0
    distanciaHistograma = 0.0
    distanciaTextura = 0.0
    movimento = 0
    distanciaHistogramaParte1 = 0.0
    distanciaTexturaParte1 = 0.0
    movimentoParte1 = 0
    distanciaHistogramaParte2 = 0.0
    distanciaTexturaParte2 = 0.0
    movimentoParte2 = 0
    distanciaHistogramaParte3 = 0.0
    distanciaTexturaParte3 = 0.0
    movimentoParte3 = 0

    def __init___(self,
                  frame,
                  frameanterior,
                  numero,
                  distanciaHistograma,
                  distanciaTextura,
                  movimento,
                  distanciaHistogramaParte1,
                  distanciaTexturaParte1,
                  movimentoParte1,
                  distanciaHistogramaParte2,
                  distanciaTexturaParte2,
                  movimentoParte2,
                  distanciaHistogramaParte3,
                  distanciaTexturaParte3,
                  movimentoParte3):

        self.frame = frame
        self.frameanterior = frameanterior
        self.numero = numero
        self.distanciaHistograma = distanciaHistograma
        self.distanciaTextura = distanciaTextura
        self.movimento = movimento
        self.distanciaHistogramaParte1 = distanciaHistogramaParte1
        self.distanciaTexturaParte1 = distanciaTexturaParte1
        self.movimentoParte1 = movimentoParte1
        self.distanciaHistogramaParte2 = distanciaHistogramaParte2
        self.distanciaTexturaParte2 = distanciaTexturaParte2
        self.movimentoParte2 = movimentoParte2
        self.distanciaHistogramaParte3 = distanciaHistogramaParte3
        self.distanciaTexturaParte3 = distanciaTexturaParte3
        self.movimentoParte3 = movimentoParte3

    # Geters

    def getFrame(self):
        return self.frame

    def getFrameAnterior(self):
        return self.frameanterior

    def getNumero(self):
        return self.numero

    def getDistanciaHistograma(self):
        return self.distanciaHistograma

    def getDistanciaTextura(self):
        return self.distanciaTextura

    def getMovimento(self):
        return self.movimento

    def getDistanciaHistogramaParte1(self):
        return self.distanciaHistogramaParte1

    def getDistanciaTexturaParte1(self):
        return self.distanciaTexturaParte1

    def getMovimentoParte1(self):
        return self.movimentoParte1

    def getDistanciaHistogramaParte2(self):
        return self.distanciaHistogramaParte2

    def getDistanciaTexturaParte2(self):
        return self.distanciaTexturaParte2

    def getMovimentoParte2(self):
        return self.movimentoParte2

    def getDistanciaHistogramaParte3(self):
        return self.distanciaHistogramaParte1

    def getDistanciaTexturaParte3(self):
        return self.distanciaTexturaParte1

    def getMovimentoParte3(self):
        return self.movimentoParte1

    # Seters
    
    def setFrame(self, frame):
        self.frame = frame

    def setFrameAnterior(self, frameanterior):
        self.frameanterior = frameanterior

    def setNumero(self, numero):
        self.numero = numero

    def setDistanciaHistograma(self, distanciaHistograma):
        self.distanciaHistograma = distanciaHistograma

    def setDistanciaTextura(self, distanciaTextura):
        self.distanciaTextura = distanciaTextura

    def setMovimento(self, movimento):
        self.movimento = movimento

    def setDistanciaHistogramaParte1(self, distanciaHistogramaParte1):
        self.distanciaHistogramaParte1 = distanciaHistogramaParte1

    def setDistanciaTexturaParte1(self, distanciaTexturaParte1):
        self.distanciaTexturaParte1 = distanciaTexturaParte1

    def setMovimentoParte1(self, movimentoParte1):
        self.movimentoParte1 = movimentoParte1

    def setDistanciaHistogramaParte2(self, distanciaHistogramaParte2):
        self.distanciaHistogramaParte2 = distanciaHistogramaParte2

    def setDistanciaTexturaParte2(self, distanciaTexturaParte2):
        self.distanciaTexturaParte2 = distanciaTexturaParte2

    def setMovimentoParte2(self, movimentoParte2):
        self.movimentoParte2 = movimentoParte2

    def setDistanciaHistogramaParte3(self, distanciaHistogramaParte3):
        self.distanciaHistogramaParte3 = distanciaHistogramaParte3

    def setDistanciaTexturaParte3(self, distanciaTexturaParte3):
        self.distanciaTexturaParte3 = distanciaTexturaParte3

    def setMovimentoParte3(self, movimentoParte3):
        self.movimentoParte3 = movimentoParte3

    def printFrame(self):

        print'%.2f;%.2f;%.2f; %.2f;%.2f;%.2f; %.2f;%.2f;%.2f; %.2f;%.2f;%.2f' % (self.distanciaHistograma,
                                                                                          self.distanciaTextura,
                                                                                          self.movimento,
                                                                                          self.distanciaHistogramaParte1,
                                                                                          self.distanciaTexturaParte1,
                                                                                          self.movimentoParte1,
                                                                                          self.distanciaHistogramaParte2,
                                                                                          self.distanciaTexturaParte2,
                                                                                          self.movimentoParte2,
                                                                                          self.distanciaHistogramaParte3,
                                                                                          self.distanciaTexturaParte3,
                                                                                          self.movimentoParte3)




    
