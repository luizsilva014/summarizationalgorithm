﻿import numpy as np
import cv2
import math
import RegionSeed as rs
import Frame as fr
from skimage import feature
import winsound

##################
# Define funções #
##################

def dist_euclidiana(v1, v2):

	dim, soma = len(v1), 0

	for i in range(dim):
		soma += math.pow(v1[i] - v2[i], 2)

	return math.sqrt(soma)

def abstrair_caracteristicas(img1, img2):

        # Gera histograma do frame atual e do frame anterior
        histogramaAtual = cv2.calcHist([img1],[0],None,[256],[0,256])
        histogramaAnterior = cv2.calcHist([img2],[0],None,[256],[0,256])

        # Calcula histograma da textura atual e da textura anterior
        t1 = feature.local_binary_pattern(img1, 8, 1)
        texturaAtual, bins = np.histogram(t1.ravel(),256,[0,256])
                
        t2 = feature.local_binary_pattern(img2, 8, 1)
        texturaAnterior, bins = np.histogram(t2.ravel(),256,[0,256])

        # Calcula o Movimento da Imagem
        soma = 0;
        movimento = img1 - img2
        largura, altura = movimento.shape      
        for i in range (0, largura):
                for j in range (0, altura):
                        soma = soma + movimento[i][j]

        return dist_euclidiana(histogramaAtual, histogramaAnterior), dist_euclidiana(texturaAtual, texturaAnterior), soma

#####################
# Fim de definições #
#####################

# Tenta abrir o video
cap = cv2.VideoCapture('DataBase/Jogo3.mp4')
lVideoAberto = cap.isOpened()

lLinhaDinamica = False
cPath = "Frames\Jogo3LinhaFixa"

if (lVideoAberto == False):

    print("Erro ao abrir o vídeo!")

else:

    # Variaveis de controle
    frameCinzaAtual = None
    frameCinzaAnterior = None
    f1 = fr.Frame()

    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    nContador = 0
    nContadorFrames = 1
    print("Histograma;textura;movimento")

    while(True):

        # Le de trinta em trinta
        nContadorFrames = nContadorFrames + 30
        cap.set(cv2.CAP_PROP_POS_FRAMES, nContadorFrames)

        # Captura frame a frame, caso o retorno seja verdadeiro continua
        retorno, frame = cap.read()
        
        if retorno == True:

            # Contador de frames
            nContador = nContador + 1

            #Converte o frame para cinza e calcula o histograma
            frameCinzaAtual = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imwrite(cPath+'\Frame'+str(nContador)+'.bmp', frame)

            if (frameCinzaAnterior is not None):

                # seta propriedade para o objeto do tipo frame
                f1.setFrame(frameCinzaAtual)
                f1.setFrameAnterior(frameCinzaAnterior)
                f1.setNumero(cap.get(cv2.CAP_PROP_POS_FRAMES))

                # Abstrai caracteristicas da imagem inteira
                distanciaHistograma, distanciaTextura, movimento = abstrair_caracteristicas(frameCinzaAtual, frameCinzaAnterior)
                f1.setDistanciaHistograma(distanciaHistograma)
                f1.setDistanciaTextura(distanciaTextura)
                f1.setMovimento(movimento)

                # Divide imgem Atual
                r1 = rs.RegionSeed(frameCinzaAtual)
                if (lLinhaDinamica == True):
                        n1, n2, n3 = r1.delimitarImagemLinhaDinamica()
                else:
                        n1, n2, n3 = r1.delimitarImagemLinhaFixa()
                
                parte1Atual, parte2Atual, parte3Atual = r1.dividirImagem(n1, n2, n3)

                cv2.imwrite(cPath+'\Frame'+str(nContador)+'p1.bmp', parte1Atual)
                cv2.imwrite(cPath+'\Frame'+str(nContador)+'p2.bmp', parte2Atual)
                cv2.imwrite(cPath+'\Frame'+str(nContador)+'p3.bmp', parte3Atual)

                # Divide imgem Anterior
                r2 = rs.RegionSeed(frameCinzaAnterior)
                parte1Anterior, parte2Anterior, parte3Anterior = r2.dividirImagem(n1, n2, n3)

                # Abstrai caracteristicas da imagem separadamente (Ceu, meio e chao)
                distanciaHistogramaParte1, distanciaTexturaParte1, movimentoParte1 = abstrair_caracteristicas(parte1Atual, parte1Anterior)
                f1.setDistanciaHistogramaParte1(distanciaHistogramaParte1)
                f1.setDistanciaTexturaParte1(distanciaTexturaParte1)
                f1.setMovimentoParte1(movimentoParte1)

                distanciaHistogramaParte2, distanciaTexturaParte2, movimentoParte2 = abstrair_caracteristicas(parte2Atual, parte2Anterior)
                f1.setDistanciaHistogramaParte2(distanciaHistogramaParte2)
                f1.setDistanciaTexturaParte2(distanciaTexturaParte2)
                f1.setMovimentoParte2(movimentoParte2)

                distanciaHistogramaParte3, distanciaTexturaParte3, movimentoParte3 = abstrair_caracteristicas(parte3Atual, parte3Anterior)
                f1.setDistanciaHistogramaParte3(distanciaHistogramaParte3)
                f1.setDistanciaTexturaParte3(distanciaTexturaParte3)
                f1.setMovimentoParte3(movimentoParte3)

                f1.printFrame()

            frameCinzaAnterior = frameCinzaAtual

        else:
            print("fim do video!")
            break

cap.release()
cv2.destroyAllWindows()
